#!/bin/bash -ex

# CONFIG
prefix="Snagit"
suffix=""
munki_package_name="Snagit2025"
display_name="Snagit 2025 - BYOL"
icon_name=""
category="Utilities"
description="Snagit is a screenshot program that captures video display and audio output. "
url="https://download.techsmith.com/snagitmac/releases/Snagit.dmg"

# download it (-L: follow redirects)
#curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' "${url}"
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"


mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $2 } '`

## Unpack
#unzip app.zip
# /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg

# make DMG from the inner prefpane
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R "$app_in_dmg" build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$app_in_dmg"/Contents/Info.plist`
#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`

 hdiutil detach "${mountpoint}"

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist

plutil -replace BundleIsRelocatable -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --scripts scripts --version ${version} app.pkg

rm -rf Component-${munki_package_name}.plist

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info
#version=`defaults read "${plist}" version`
minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "${minver}"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" blocking_applications -array "Snagit 2025.app"
defaults write "${plist}" unattended_install -bool YES
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" unattended_install -bool TRUE

# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
