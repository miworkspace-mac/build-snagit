#!/bin/bash -ex

url="https://download.techsmith.com/snagitmac/releases/Snagit.dmg"

# download it (-L: follow redirects)
#curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' "${url}"
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"


mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $2 } '`

## Unpack
#unzip app.zip
# /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg

# make DMG from the inner prefpane
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R "$app_in_dmg" build-root/Applications

# Obtain version info
VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$app_in_dmg"/Contents/Info.plist`
rm -rf build-root
rm -rf app.dmg

if [ "x${VERSION}" != "x" ]; then
    echo "${VERSION}"
    echo "${VERSION}" > current-version
fi

# Update to handle distributed builds
if cmp current-version old-version; then
    # Versions are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Versions are different - copy version, exit 0 to trigger build job
    cp current-version old-version
    exit 0
fi